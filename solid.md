# SOLID stands for 5 principles when doing oop and even functional programming also.

#### These principles are introduced to make easier software designs, which will be easier to maintain in the future.

## 5 principles are -

- Single responsibility
- Open/Closed principles
- Liskov substitution
- Interface segregation
- Dependency inversion

## 1- Single responsibility

Single responsibility means one class or one function should do only one job, or we can say one class or one function should have only one responsibility.

If we want to change one class, it should be changed because of its reason.

#### eg.-

```
class work {
    constructor(path){
        const data = getData(path)
        saveData(data);
    }
    getData(path){
        const data = fetch(path)
        return data
    }
    saveData(data){
        const arr = Object.entries(data)
        return data
        fs.writeFile("..//output/data.json", data, "utf", (err)=>{
            if(err){
                console.log(err)
            }
        })
    }
}
```

In this code one function is doing more than one job, the saveData function is also converting the data, and then writing it into a file.

```
class work{
    constructor(path){
        this.path = path
    }
    getData(path){
        const data = fetch(this.path)
        return data
    }
}

class savedata{
    constructor(data){
        this.data = data
    }
    save(){
        fs.writeFile("..//output/data.json", data, "utf", (err)=>{
            if(err){
                console.log(err)
            }
        })
    }
}
```
This is the right way, one function or class is doing one job only.
## 2- Open/Closed Principles -

It simply means open for extension and closed for modification, seems like a little bit confusing, let's break it down, it means we can add a new feature to app or class, but we cant not modify the existing feature.

#### Example-

```
class calculator{
    add(){

    }
    sub(){

    }
    mul(){

    }
}

class scientific_Cal extends calculator{
    sin(){

    }
    cos(){

    }
    tan(){

    }
}
```

 In this program we did not modify our class, but we just inherit it and add more features to it, so this is the way, we can satisfy the open/closed principles.

## 3- Liskov Substitution principles

LSP principle says child class should be able to substitute the parent class, or base class must be replaced by child class without changing their behavior.


## 4- Interface Segregation principle-

Multiple interfaces for multiple clients is better than one generalize interface for all client.


## 5- Dependency inversion

 High-level class should not depend on low-level class, it means there shouldn't be tight coupling between high-level class or low-level classes.
 So the high-level module and low-level module depends on the same abstraction.


# References- 
1. [Solid basic](https://itnext.io/solid-principles-explanation-and-examples-715b975dcad4)
1. [single_ressponsibility](https://scotch.io/bar-talk/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)



